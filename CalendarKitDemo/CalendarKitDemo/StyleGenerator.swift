import CalendarKit
import DateToolsSwift

class StyleGenerator {
    var flag : Bool = false
  static func defaultStyle() -> CalendarStyle {
    return CalendarStyle()
  }

  static func darkStyle() -> CalendarStyle {
    let orange = UIColor.orange
    let dark = UIColor(white: 0.1, alpha: 1)
    let light = UIColor.lightGray
    let white = UIColor.white

    let selector = DaySelectorStyle()
    selector.activeTextColor = white
    selector.inactiveTextColor = white
    selector.selectedBackgroundColor = light
    selector.todayActiveBackgroundColor = orange
    selector.todayInactiveTextColor = orange

    let daySymbols = DaySymbolsStyle()
    daySymbols.weekDayColor = white
    daySymbols.weekendColor = light

    let swipeLabel = SwipeLabelStyle()
    swipeLabel.textColor = white

    let header = DayHeaderStyle()
    header.daySelector = selector
    header.daySymbols = daySymbols
    header.swipeLabel = swipeLabel
    header.backgroundColor = dark

    let timeline = TimelineStyle()
    timeline.timeIndicator.color = orange
    timeline.lineColor = light
    timeline.timeColor = light
    timeline.backgroundColor = dark

    let style = CalendarStyle()
    style.header = header
    style.timeline = timeline

    return style
  }
    
    
    static func periodTrackerCalenderStyle(periodDate: Date, currentDate: Date) -> CalendarStyle {
        let newPeriodDate = periodDate
        
        
        
        //Cycle length suppose here the cycle length is 20 Days
        
        
        
        
        //Starting and ending dates of period. Here the gap is 7 days but lator on it will depends on the period length
        let periodStartingDate = newPeriodDate
        let periodEndingDate = newPeriodDate.add(7.days)
         let selector = DaySelectorStyle()
        //This is the timeperiod of the period
        let periodTimePeriod = TimePeriod(beginning: periodStartingDate, end: periodEndingDate)
        
        
        
        
        if periodTimePeriod.contains(currentDate, interval: Interval.closed)  {
            print("The current date lies in the time period")
            selector.activeTextColor = UIColor.white
            selector.inactiveTextColor = UIColor.gray
            selector.selectedBackgroundColor = UIColor.purple
            selector.todayActiveBackgroundColor = UIColor.purple
            selector.todayInactiveTextColor = UIColor.gray
            
            
        }else {
            
            print("The Current date does no lies in the time period")
            selector.activeTextColor = UIColor.gray
            selector.inactiveTextColor = UIColor.gray
            selector.selectedBackgroundColor = UIColor.green
            selector.todayActiveBackgroundColor = UIColor.white
            selector.todayInactiveTextColor = UIColor.gray
            
        }
        
        
        let daySymbols = DaySymbolsStyle()
        daySymbols.weekDayColor = UIColor.gray
        daySymbols.weekendColor = UIColor.gray
        
        let swipeLabel = SwipeLabelStyle()
        swipeLabel.textColor = UIColor.black
        
        let header = DayHeaderStyle()
        header.daySelector = selector
        header.daySymbols = daySymbols
        header.swipeLabel = swipeLabel
        header.backgroundColor = UIColor.clear
        
        let timeline = TimelineStyle()
        timeline.timeIndicator.color = UIColor.white
        timeline.lineColor = UIColor.white
        timeline.timeColor = UIColor.white
        timeline.backgroundColor = UIColor.clear
        
        let style = CalendarStyle()
        style.header = header
        style.timeline = timeline
        
        return style
        
        
    }
}
